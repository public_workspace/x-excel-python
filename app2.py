import tkinter as tk
from tkinter import filedialog
import pandas as pd
from openpyxl import load_workbook
from openpyxl.styles import Font

def process_excel(file_path):
    header_row = 3
    df = pd.read_excel(file_path, header=header_row)
    selected_columns = df[['ตัวเลข', 'บน', 'ล่าง', 'ผู้ซื้อ']]
    selected_columns = selected_columns.dropna()
    selected_columns_data = selected_columns.values

    listdata = []
    groupdata = ''
    selected_columns_data = selected_columns_data.tolist()
    while len(selected_columns_data) > 0 :
        if not groupdata:
            groupdata = selected_columns_data[0][0]

        _down = 0
        _up = 0
        for item in selected_columns_data:  # Use a copy of original_sequence to iterate
            if groupdata and groupdata == item[0]:
                _up = _up + float(item[1])
                _down = _down + float(item[2])
                listdata.append(item)
                selected_columns_data.remove(item)

        listdata.append(['รวม',_up,_down])
        listdata.append([])
        groupdata = ''
        _down = 0
        _up = 0 

    headers = ["number", "up", "down", "name"]
    df_result = pd.DataFrame(listdata, columns=headers)

    save_file(df_result)


def save_file(df_result):
    file_path = filedialog.asksaveasfilename(defaultextension=".xlsx", filetypes=[("Excel files", "*.xlsx")])
    if file_path:
        df_result.to_excel(file_path, index=False)

        # Open the saved file with openpyxl
        workbook = load_workbook(file_path)
        sheet = workbook.active

        # Set the font size to 18 for all cells in the sheet
        for row in sheet.iter_rows():
            for cell in row:
                cell.font = Font(size=18)

        # Save the changes
        workbook.save(file_path)
        label_result.config(text=f"Processing complete. File saved at '{file_path}'.")


def browse_file():
    file_path = filedialog.askopenfilename(filetypes=[("All files", "*.*")])
    if file_path:
        process_excel(file_path)

# Create the main window
window = tk.Tk()
window.title("Excel Processor")

# Create and pack widgets
label_instruction = tk.Label(window, text="Select an Excel file to process:")
label_instruction.pack(pady=10)

button_browse = tk.Button(window, text="Browse", command=browse_file)
button_browse.pack(pady=10)

label_result = tk.Label(window, text="")
label_result.pack(pady=10)

# Start the GUI event loop
window.mainloop()

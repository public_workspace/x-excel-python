import tkinter as tk
from tkinter import filedialog
import pandas as pd
from openpyxl import load_workbook
from openpyxl.styles import Font

def process_excel(file_path):
    header_row = 3
    df = pd.read_excel(file_path, header=header_row)
    selected_columns = df[['ตัวเลข', 'บน-ตรง', 'บน-โต๊ด', 'ล่าง', 'ผู้ซื้อ']]
    selected_columns = selected_columns.dropna()
    selected_columns_data = selected_columns.values

    listdata = []
    groupdata = ''
    selected_columns_data = selected_columns_data.tolist()

    datas = group_and_sort_items(selected_columns_data);

    listdata = [];
    headers = ["number", "tod", "tong", "name"]
    count = 0;
    for key, value in datas.items():
        head_number = [f"เลขหลัก {key}" ,"" , "" , ""]
        listdata.append(head_number)

        _tod = 0;
        _tong = 0;
        for item in value :
            count = count + 1;
            listdata.append([item[0] , item[1] , item[2] , item[4]])
            _tod = _tod + item[1]
            _tong = _tong + item[2]

        listdata.append(["รวม" , _tod , _tong , ""])
        listdata.append([''] * len(headers))
        

    listdata.append(["จำนวน" , f"process : {count}" , f" real : {len(selected_columns.values)}" , ""])
    df_result = pd.DataFrame(listdata, columns=headers)

    save_file(df_result)


def save_file(df_result):
    file_path = filedialog.asksaveasfilename(defaultextension=".xlsx", filetypes=[("Excel files", "*.xlsx")])
    if file_path:
        df_result.to_excel(file_path, index=False)

        # Open the saved file with openpyxl
        workbook = load_workbook(file_path)
        sheet = workbook.active

        # Set the font size to 18 for all cells in the sheet
        for row in sheet.iter_rows():
            for cell in row:
                cell.font = Font(size=18)

        # Save the changes
        workbook.save(file_path)
        label_result.config(text=f"Processing complete. File saved at '{file_path}'.")


def group_and_sort_items(items):

    grouped_items = {}
    current_key = '';
    count = 0;
    while len(items) > 0 :
        for item in items:
            count = count + 1;
    
            num = item[0].split('-')[1].strip()
            key = ''.join(sorted(str(num)))
            if key not in grouped_items and not current_key:
                grouped_items[key] = [];
                current_key = key;

            if key in grouped_items :
                grouped_items[key].append(item)
                items.remove(item);

        # reset process
        current_key = '';

    sorted_dict = dict(sorted(grouped_items.items()))
    return sorted_dict;
    





def browse_file():
    file_path = filedialog.askopenfilename(filetypes=[("All files", "*.*")])
    if file_path:
        process_excel(file_path)

# Create the main window
window = tk.Tk()
window.title("Excel Processor")

# Create and pack widgets
label_instruction = tk.Label(window, text="Select an Excel file to process:")
label_instruction.pack(pady=10)

button_browse = tk.Button(window, text="Browse", command=browse_file)
button_browse.pack(pady=10)

label_result = tk.Label(window, text="")
label_result.pack(pady=10)

# Start the GUI event loop
window.mainloop()
